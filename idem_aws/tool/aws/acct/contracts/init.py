from typing import Any
from typing import Dict


async def sig_modify(
    hub, name: str, raw_profile: Dict[str, Any], new_profile: Dict[str, Any]
):
    ...
