# Find leftover VPCs
orphan_vpcs:
  exec.run:
    - path: aws.ec2.vpc.list
    - kwargs:
        name: null
        filters:
          - name: "tag:Name"
            values:
              - "idem-fixture-vpc-*"

# Remove leftover VPCs
#!require:orphan_vpcs
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan_vpcs}') %}

# Remove any leftover resources attached to the vpc
cleanup-{{ resource['resource_id'] }}:
  aws.ec2.vpc.absent:
    - resource_id: {{ resource['resource_id'] }}

{% endfor %}
#!END
