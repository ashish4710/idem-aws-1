import copy
import time
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-vpc-peering-connection-" + str(int(time.time())),
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, aws_ec2_vpc, aws_ec2_vpc_2, __test, cleanup):
    # Test create with and without a flag
    global PARAMETER
    ctx["test"] = __test

    PARAMETER["peer_owner_id"] = aws_ec2_vpc["OwnerId"]
    PARAMETER["peer_vpc_id"] = aws_ec2_vpc["VpcId"]
    PARAMETER["vpc_id"] = aws_ec2_vpc_2["VpcId"]
    PARAMETER["peer_region"] = ctx["acct"].get("region_name")
    PARAMETER["tags"] = {"idem-test-key-name": PARAMETER["name"]}
    PARAMETER["status"] = "active"

    ret = await hub.states.aws.ec2.vpc_peering_connection.present(
        ctx,
        **PARAMETER,
    )

    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                "aws.ec2.vpc_peering_connection", PARAMETER["name"]
            )
            == ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                "aws.ec2.vpc_peering_connection", PARAMETER["name"]
            )
            + hub.tool.aws.comment_utils.resource_status_updated_comment(
                "aws.ec2.vpc_peering_connection",
                PARAMETER["name"],
                resource.get("status"),
            )
            == ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["status"] == resource.get("status")
    assert PARAMETER["vpc_id"] == resource.get("vpc_id")
    assert PARAMETER["peer_vpc_id"] == resource.get("peer_vpc_id")
    assert PARAMETER["peer_owner_id"] == resource.get("peer_owner_id")
    assert PARAMETER["peer_region"] == resource.get("peer_region")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    # Test describe
    describe_ret = await hub.states.aws.ec2.vpc_peering_connection.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret

    # Verify that the describe function output format is correct
    assert "aws.ec2.vpc_peering_connection.present" in describe_ret[resource_id]

    described_resource = describe_ret[resource_id].get(
        "aws.ec2.vpc_peering_connection.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["vpc_id"] == described_resource_map.get("vpc_id")
    assert PARAMETER["peer_vpc_id"] == described_resource_map.get("peer_vpc_id")
    assert PARAMETER["peer_owner_id"] == described_resource_map.get("peer_owner_id")
    assert PARAMETER["peer_region"] == described_resource_map.get("peer_region")
    assert PARAMETER["tags"] == described_resource_map.get("tags")
    assert PARAMETER["status"] == described_resource_map.get("status")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="update_tags", depends=["describe"])
async def test_update_tags(hub, ctx, __test):
    # Test update tags with and without a flag.
    # The tags are the only updatable property for a VPC peering connection.
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)

    ctx["test"] = __test

    new_parameter["tags"][
        f"idem-test-key-{str(int(time.time()))}"
    ] = f"idem-test-value-{str(int(time.time()))}"
    ret = await hub.states.aws.ec2.vpc_peering_connection.present(
        ctx,
        **new_parameter,
    )

    assert ret["result"], ret["comment"]
    assert ret["old_state"], ret["new_state"]

    assert ret["new_state"]["tags"] != ret["old_state"]["tags"]
    assert ret["new_state"]["tags"] == new_parameter["tags"]
    assert ret["old_state"]["tags"] == PARAMETER["tags"]
    assert ret["new_state"]["vpc_id"] == new_parameter["vpc_id"]
    assert ret["old_state"]["vpc_id"] == PARAMETER["vpc_id"]
    assert ret["new_state"]["peer_vpc_id"] == new_parameter["peer_vpc_id"]
    assert ret["old_state"]["peer_vpc_id"] == PARAMETER["peer_vpc_id"]
    assert ret["new_state"]["peer_owner_id"] == new_parameter["peer_owner_id"]
    assert ret["old_state"]["peer_owner_id"] == PARAMETER["peer_owner_id"]
    assert ret["new_state"]["peer_region"] == new_parameter["peer_region"]
    assert ret["old_state"]["peer_region"] == PARAMETER["peer_region"]

    # Status is active from the changes in the previous test (create) on which this test depends on.
    # The status is not updatable when using the present function - it is only used when resource is created in real
    assert "active" == ret["new_state"]["status"]
    assert "active" == ret["old_state"]["status"]

    added = {
        tag: new_parameter["tags"][tag]
        for tag in new_parameter["tags"]
        if tag not in PARAMETER["tags"]
    }
    removed = {
        tag: PARAMETER["tags"][tag]
        for tag in PARAMETER["tags"]
        if tag not in new_parameter["tags"]
    }
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_tags_comment(removed, added)
            + hub.tool.aws.comment_utils.would_update_comment(
                "aws.ec2.vpc_peering_connection",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_tags_comment(removed, added)
            + hub.tool.aws.comment_utils.update_comment(
                "aws.ec2.vpc_peering_connection",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="absent", depends=["update_tags"])
async def test_absent(hub, ctx, __test):
    # Test delete with and without a flag
    ctx["test"] = __test

    ret = await hub.states.aws.ec2.vpc_peering_connection.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]

    old_resource = ret["old_state"]
    assert PARAMETER["tags"] == old_resource.get("tags")
    assert PARAMETER["vpc_id"] == old_resource.get("vpc_id")
    assert PARAMETER["peer_vpc_id"] == old_resource.get("peer_vpc_id")
    assert PARAMETER["peer_owner_id"] == old_resource.get("peer_owner_id")
    assert PARAMETER["peer_region"] == old_resource.get("peer_region")
    assert PARAMETER["status"] == old_resource.get("status")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                "aws.ec2.vpc_peering_connection", PARAMETER["name"]
            )
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                "aws.ec2.vpc_peering_connection", PARAMETER["name"]
            )
            == ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    # Test already absent
    ctx["test"] = __test
    ret = await hub.states.aws.ec2.vpc_peering_connection.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            "aws.ec2.vpc_peering_connection", PARAMETER["name"]
        )
        == ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_absent_with_none_resource_id(hub, ctx):
    vpc_peering_connection_temp_name = "idem-test-vpc-peering-connection-" + str(
        int(time.time())
    )

    ret = await hub.states.aws.ec2.vpc_peering_connection.absent(
        ctx, name=vpc_peering_connection_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            "aws.ec2.vpc_peering_connection", vpc_peering_connection_temp_name
        )
        == ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.ec2.vpc_peering_connection.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]
