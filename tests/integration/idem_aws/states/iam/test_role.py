import copy
import json
import time
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_role(hub, ctx):
    role_temp_name = "idem-test-role-" + str(int(time.time()))
    assume_role_policy_document = '{"Statement": [{"Action": "sts:AssumeRole", "Effect": "Allow", "Principal": {"Service": "spot.amazonaws.com"}}], "Version": "2012-10-17"}'
    description = "Idem IAM role integration test"
    max_session_duration = 3700
    tags = {"Name": role_temp_name}

    # Create IAM role with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.iam.role.present(
        test_ctx,
        name=role_temp_name,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
        max_session_duration=max_session_duration,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert role_temp_name == resource.get("name")
    assert json.dumps(
        json.loads(assume_role_policy_document), separators=(", ", ": ")
    ) == resource.get("assume_role_policy_document")
    assert description == resource.get("description")
    assert max_session_duration == resource.get("max_session_duration")
    assert tags == resource.get("tags")

    # Create IAM role
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_temp_name,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
        max_session_duration=max_session_duration,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert role_temp_name == resource.get("name")
    assert json.dumps(
        json.loads(assume_role_policy_document), separators=(", ", ": "), sort_keys=True
    ) == resource.get("assume_role_policy_document")
    assert description == resource.get("description")
    assert max_session_duration == resource.get("max_session_duration")
    assert tags == resource.get("tags")
    resource_id = resource.get("resource_id")
    assert role_temp_name == resource_id

    # Verify present w/o changes does not trigger an update
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_temp_name,
        resource_id=resource_id,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
        max_session_duration=max_session_duration,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"'{role_temp_name}' already exists" in str(ret["comment"])
    assert ret["old_state"] == ret["new_state"], "Should not update existing role"

    # Describe IAM role
    describe_ret = await hub.states.aws.iam.role.describe(ctx)
    resource_key = f"iam-role-{resource_id}"
    assert resource_key in describe_ret
    assert "aws.iam.role.present" in describe_ret.get(resource_key)
    described_resource = describe_ret.get(resource_key).get("aws.iam.role.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert role_temp_name == described_resource_map.get("name")
    assert json.dumps(
        json.loads(assume_role_policy_document), separators=(", ", ": "), sort_keys=True
    ) == described_resource_map.get("assume_role_policy_document")
    assert description == described_resource_map.get("description")
    assert max_session_duration == described_resource_map.get("max_session_duration")
    assert resource_id == described_resource_map.get("resource_id")
    assert tags == described_resource_map.get("tags")

    # search  IAM role
    ret = await hub.states.aws.iam.role.search(ctx, name=role_temp_name)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    old_state = ret.get("old_state")
    assert role_temp_name == old_state.get("name")
    assert json.dumps(
        json.loads(assume_role_policy_document), separators=(", ", ": "), sort_keys=True
    ) == old_state.get("assume_role_policy_document")
    assert description == old_state.get("description")
    assert max_session_duration == old_state.get("max_session_duration")
    assert tags == old_state.get("tags")
    assert described_resource_map.get("arn") == old_state.get("arn")
    resource_id = resource.get("resource_id")
    assert role_temp_name == resource_id

    new_state = ret.get("new_state")
    assert role_temp_name == new_state.get("name")
    assert json.dumps(
        json.loads(assume_role_policy_document), separators=(", ", ": "), sort_keys=True
    ) == new_state.get("assume_role_policy_document")
    assert description == new_state.get("description")
    assert max_session_duration == new_state.get("max_session_duration")
    assert tags == new_state.get("tags")
    assert described_resource_map.get("arn") == old_state.get("arn")
    resource_id = resource.get("resource_id")
    assert role_temp_name == resource_id

    # Test updating description, max_session_duration, policy and adding tags
    description = "Idem IAM role test description updated"
    max_session_duration = 3800
    tags.update(
        {
            f"idem-test-iam-key-{str(uuid.uuid4())}": f"idem-test-iam-value-{str(uuid.uuid4())}"
        }
    )

    # Update role with test flag
    ret = await hub.states.aws.iam.role.present(
        test_ctx,
        name=role_temp_name,
        resource_id=resource_id,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
        max_session_duration=max_session_duration,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert description == resource.get("description")
    assert max_session_duration == resource.get("max_session_duration")
    assert assume_role_policy_document == resource.get("assume_role_policy_document")
    assert tags == resource.get("tags")

    # Update role with real endpoint
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_temp_name,
        resource_id=resource_id,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
        max_session_duration=max_session_duration,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert description == resource.get("description")
    assert max_session_duration == resource.get("max_session_duration")
    assert assume_role_policy_document == resource.get("assume_role_policy_document")
    assert tags == resource.get("tags")

    # Test deleting tags
    tags.pop("Name")
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_temp_name,
        resource_id=resource_id,
        assume_role_policy_document=assume_role_policy_document,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    old_resource = ret.get("old_state")
    resource = ret.get("new_state")
    assert old_resource.get("description") == resource.get("description")
    assert old_resource.get("max_session_duration") == resource.get(
        "max_session_duration"
    )
    assert tags == resource.get("tags")

    # Delete role with test flag
    ret = await hub.states.aws.iam.role.absent(
        test_ctx, name=role_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would delete aws.iam.role '{role_temp_name}'" in str(ret["comment"])

    # Delete IAM role - require name for deletion
    ret = await hub.states.aws.iam.role.absent(
        ctx, name=role_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete IAM role again
    ret = await hub.states.aws.iam.role.absent(
        ctx, name=role_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")


@pytest.mark.asyncio
async def test_role_absent(hub, ctx):
    # Delete role with and w/o detach_role_policies flag
    role_temp_name = "idem-test-role-" + str(uuid.uuid4())
    assume_role_policy_document = '{"Statement": [{"Action": "sts:AssumeRole", "Effect": "Allow", "Principal": {"Service": "spot.amazonaws.com"}}], "Version": "2012-10-17"}'
    description = "Idem IAM role integration test"
    max_session_duration = 3700
    tags = {"Name": role_temp_name}

    # Create IAM role
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_temp_name,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
        max_session_duration=max_session_duration,
        tags=tags,
    )
    assert ret["result"], ret["comment"]

    # Create role policies
    policy_names = [
        "idem-test-policy-" + str(uuid.uuid4()),
        "idem-test-policy-" + str(uuid.uuid4()),
    ]
    policies = [
        '{"Statement": [{"Action": ["ec2:CreateSubnet"], "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}',
        '{"Statement": [{"Action": ["ec2:DeleteSubnet"], "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}',
    ]

    for i in range(0, 2):
        ret = await hub.states.aws.iam.role_policy.present(
            ctx,
            name=policy_names[i],
            role_name=role_temp_name,
            policy_document=policies[i],
        )
        assert ret["result"], ret["comment"]

    # Create role policy attachments
    att_policy_names = [
        "idem-attached-policy-" + str(uuid.uuid4()),
        "idem-attached-policy-" + str(uuid.uuid4()),
    ]
    att_policies = [
        '{"Statement": [{"Action": ["ec2:CreateVpc"], "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}',
        '{"Statement": [{"Action": ["ec2:DeleteVpc"], "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}',
    ]
    policy_resource_ids = []

    for i in range(0, 2):
        ret = await hub.states.aws.iam.policy.present(
            ctx,
            name=att_policy_names[i],
            policy_document=att_policies[i],
            description="Created by test for " + att_policy_names[i],
        )
        assert ret["result"], ret["comment"]
        policy_arn = ret["new_state"].get("resource_id")
        policy_resource_ids.append(policy_arn)
        ret = await hub.states.aws.iam.role_policy_attachment.present(
            ctx,
            name=role_temp_name,
            role_name=role_temp_name,
            policy_arn=policy_arn,
        )
        assert ret["result"], ret["comment"]

    #  Should fail
    ret = await hub.states.aws.iam.role.absent(
        ctx, name=role_temp_name, resource_id=role_temp_name, detach_role_policies=False
    )
    assert not ret["result"], ret["comment"]
    assert ret["old_state"]
    assert "must detach all policies" in ret["comment"][0], ret["comment"]

    #  Should succeed
    ret = await hub.states.aws.iam.role.absent(
        ctx, name=role_temp_name, resource_id=role_temp_name, detach_role_policies=True
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"], "Successful deletion"

    # Cleanup policies
    for resource_id in policy_resource_ids:
        ret = await hub.states.aws.iam.policy.absent(
            ctx, name=att_policy_names.pop(), resource_id=resource_id
        )
        assert ret["result"], ret["comment"]
