import inspect


def test_are_lists_identical(hub):
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(None, None)
    assert hub.tool.aws.state_comparison_utils.are_lists_identical([], [])
    assert hub.tool.aws.state_comparison_utils.are_lists_identical([], None)
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(None, [])
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(["None"], ["None"])
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        ["A", "B"], ["B", "A"]
    )

    assert not hub.tool.aws.state_comparison_utils.are_lists_identical(None, ["none"])
    assert not hub.tool.aws.state_comparison_utils.are_lists_identical([], ["none"])


def test_are_list_identical_with_string_negative(hub):
    try:
        hub.tool.aws.state_comparison_utils.are_lists_identical(["hello"], "hello")
    except TypeError as e:
        assert e
        assert "expected to be of type List" in e.args[0]


def test_are_list_identical_with_dict_negative(hub):
    try:
        hub.tool.aws.state_comparison_utils.are_lists_identical(
            ["hello", "world"], {"hello": "world"}
        )
    except TypeError as e:
        assert e
        assert "expected to be of type List" in e.args[0]


def test_standardise_json(hub):
    assert hub.tool.aws.state_comparison_utils.standardise_json(None) is None
    assert hub.tool.aws.state_comparison_utils.standardise_json(inspect._empty) is None
    assert hub.tool.aws.state_comparison_utils.standardise_json({}) is None


def test_standardise_json_with_list_negative(hub):
    try:
        assert hub.tool.aws.state_comparison_utils.standardise_json(["hello"])
    except TypeError as e:
        assert e
        assert "Expecting string or dictionary" in e.args[0]
