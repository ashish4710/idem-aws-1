import copy


def test_is_subscription_filter_updated(hub):
    # When current state is empty and desired state is not empty. is_subscription_filter_updated should return True.
    current_state = {}
    desired_state = {
        "name": "idem-test-subscription-filter",
        "log_group_name": "idem-test-log-group",
        "filter_pattern": "ERROR",
        "destination_arn": "arn:aws:lambda:region:123456789123:function:helloworld",
        "role_arn": "value1",
    }
    result = hub.tool.aws.logs.subscription_filter_utils.is_subscription_filter_updated(
        current_state, desired_state
    )
    assert result

    # When current state is same as desired state. is_subscription_filter_updated should return False.
    current_state = copy.deepcopy(desired_state)
    result = hub.tool.aws.logs.subscription_filter_utils.is_subscription_filter_updated(
        current_state, desired_state
    )
    assert not result

    # When desired state contains a new non-empty parameter. is_subscription_filter_updated should return True.
    desired_state["distribution"] = "value2"
    result = hub.tool.aws.logs.subscription_filter_utils.is_subscription_filter_updated(
        current_state, desired_state
    )
    assert result
